import React, { useState } from 'react';
import firebase from '../../firebase';

async function buildParty(props) {
  const partiesRef = firebase.firestore().collection('panParties');
  const { user, setPartyLink, setLoading, auth, raw } = props;
  const oneRef = partiesRef.doc()
  const partyData = {
    ...raw,
    hostId: user.uid,
  }

  await oneRef.set(partyData);

  const partyLink = `${window.location.origin}/order-page?partyId=${oneRef.id}&partyName=${raw.name}`;

  setPartyLink(partyLink);
}

function CreateParty(props) {

  const [partyLink, setPartyLink] = useState(undefined)

  return (

    <div className="d-flex flex-column h-100">
      <header className="p-1">
        <h1 className="d-flex justify-content-between">
          <span>Create Party</span>
          <button
            onClick={() => {
              firebase.auth().signOut()
            }}
            className="btn btn-info btn-sm"
          >
            sign out
          </button>
        </h1>
        <a className="bold-weight" href={'/login'} > dashboard </a>
      </header>
      <form
        onSubmit={(event) => {
          event.preventDefault();
          const raw = {};
          for (let index = 0; index < event.target.elements.length; index++) {
            const element = event.target.elements[index];
            if (element.name) {
              raw[element.name] = element.value;
            }
          }
          buildParty({ raw, setPartyLink, ...props })
        }}
        className="flex-1 row no-gutters scroll-y">
        <div className="col-md-6">
          <div
            className="h-100 d-flex flex-column px-1"
          >
            <label className="text-left m-0" htmlFor="order">type party name</label>
            <textarea name="name" className="form-control input-lg mb-1"></textarea>
            <label className="text-left m-0" htmlFor="order">type support email</label>
            <textarea name="supportEmail" className="form-control input-lg mb-1"></textarea>

          </div>
        </div>
        <div className="col-md-6">
          <div
            className="h-100 d-flex scroll-y flex-column px-1"
          >

            <div>
              <p>  once you click submit you will get a link.</p>
              <p> share that link to people you want to invie.</p>
              <p>  they will fill out a form with their order.</p>
              <p>  as your guest submit orders </p>
              <p>  you can check back <a href="/login">here</a></p>
              <p>  for a report of their orders by  store</p>
            </div>

            {
              !!partyLink &&
              <>
                <a className="bold-weight" href={partyLink} > partyLink </a>
                <a className="little-size" href={partyLink} > {partyLink} </a>
                <br />
              </>
            }
            <input type="submit" className="btn btn-success my-1" />
          </div>
        </div>
      </form>

    </div>

  );
}

export default CreateParty;



import React from 'react';
import firebase from '../../firebase';


function HomeSplash(props) {
  return (
    <div className="d-flex h-100 flex-column">
      <div className='new-header d-flex p-1'>
        <div className="new-header-card">
          <a className="bold-weight" href={'/login'} > Start here </a>
        </div>
        <div className="new-header-card">
          <span className='header-span' >PAN PARTY</span>
        </div>
        <div className="new-header-card d-flex justify-content-end">
          <img style={{ height: 50, width: 50 }} src="/ms-icon-310x310.png" />
        </div>
      </div>
      <div className="flex-1 scroll-y mx-1">
        <div className="d-flex info-div">
          <article className='d-flex' >
            <h1 >
              <p> Create a Party</p>
            </h1>
            <p className="bolder-weight" >
              This will generate a link that you can share with your party guest
            </p>
          </article>
          <article className='d-flex' >
            <h1 >
              <p> Share  Party Link</p>
            </h1>
            <p className="bolder-weight" >
              That link will take your guest to a order form where they can submit their order and select restaurants to fulfill the order
            </p>
          </article>
          <article className='d-flex' >
            <h1 >
              <p> View Guest Orders</p>
            </h1>
            <p className="bolder-weight" >
              as guest submit their orders pan party will collect and organize the contact information of the restaurants , that your party guest selected for their orders
            </p>
          </article>
        </div>
        <div className="d-flex info-div">
          <article className='d-flex' >
            <h1 >
              <p>Place Guest Orders</p>
            </h1>
            <p className="bolder-weight" >
              now you have everthing you need to make arrangments and coordinate,guest orders, with the selected restaurants
            </p>
          </article>
        </div>
        <article className='d-flex top-box' >
          <p className="bold-weight" >
            Pan Party helps teams coordinate virtual pizza ( or any other kind of take out food) party.
         </p>
          <p className="bold-weight" >
            Party guest are able to submit orders from any restaurant.
         </p>
          <p className="bold-weight" >
            Pan Party will then organize all the restaurant contact information
            from orders submitted by your party guest.
         </p>
        </article>
        <br />
        <br />
      </div>
    </div>
  );
}

export default HomeSplash;

import React from 'react';
import { useGetParties } from '../../hooks/useGetParties'
import firebase from '../../firebase';

function DashBoard(props) {
  const { parties = [] } = props;
  console.log("DashBoard -> parties", parties)
  useGetParties(props)
  return (
    <div className="d-flex h-100 flex-column">

      <div className='new-header d-flex p-1 alt'>
        <div className="new-header-card">
          <a className="bold-weight" href={'/create-party'} > make new party </a>
        </div>
        <div className="new-header-card">
          <span className='header-span' >DashBoard</span>
        </div>
        <div className="d-flex spready justify-content-end">

          <button
            onClick={() => {
              firebase.auth().signOut()
            }}
            className="btn btn-info btn-sm"
          >
            sign out
          </button>
        </div>
      </div>

      <div className="flex-1 scroll-y p-1">
        {parties.map(party => {
          const partyLink = `${window.location.origin}/order-page?partyId=${party.id}&partyName=${party.name}`;
          const guestOrdersLink = `${window.location.origin}/view-orders?partyId=${party.id}&partyName=${party.name}`;
          const guestOrdersByStoreLink = `${window.location.origin}/view-orders-by-store?partyId=${party.id}&partyName=${party.name}`;
          return (
            <div className='mb-3' key={party.id}>
              <div className='row'>
                <div className="col-md-6">
                  <h1>{party.name}</h1>
                  <a className="bolder-text" href={partyLink} > party link </a>
                </div>
                <div className="col-md-6">
                  <a href={guestOrdersLink} ><p className='m-0 view-orders-link'> view guest orders by email </p></a>
                  <a href={guestOrdersByStoreLink}  ><p className='m-0 view-orders-link'> view guest orders by store</p></a>
                </div>
              </div>
              <a className="little-size" href={partyLink} > {partyLink} </a>
              <hr />
            </div>
          )
        })}
      </div>
    </div>
  );
}

export default DashBoard;

import React from 'react';
import firebase from '../../firebase';

function ForgotPasswordForm(props) {

  return (
    <form
      onSubmit={(event) => {
        event.preventDefault();
        const raw = {};
        for (let index = 0; index < event.target.elements.length; index++) {
          const element = event.target.elements[index];
          if (element.name) {
            raw[element.name] = element.value;
          }
        }
        const { email } = raw;
        if (email ) {
          firebase.auth().sendPasswordResetEmail(email)
        }

      }}
    >
      <label className="m-0 p-0" htmlFor="email">email</label>
      <input type="text" name="email" className="form-control input-lg" />
      <br />
      <input className="btn btn-info boldest-text" type="submit" value="Send Reset Email" />

    </form>
  )

}

function ForgotPassword(props) {
  return (
    <div className="d-flex h-100 flex-column">
      <div className='new-header d-flex p-1'>
        <div className="new-header-card">
        <a href="/login" className="bold-weight">Login Page </a>
        </div>
        <div className="new-header-card">
          <span className='header-span' >PAN PARTY</span>
        </div>
        <div className="new-header-card d-flex justify-content-end">
          <img style={{ height: 50, width: 50 }} src="/ms-icon-310x310.png" />
        </div>
      </div>

      <div className="flex-1 scroll-y d-flex flex-column justify-content-center align-items-center">
        <ForgotPasswordForm />
      </div>
    </div>
  );
}

export default ForgotPassword;

import React from 'react';
import Login from './Login';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import DashBoard from './DashBoard';
import CreateParty from './CreateParty';
import OrderPage from './OrderPage/';
import ViewOrders from './ViewOrders';
import ViewOrdersByStore from './ViewOrdersByStore';
import HomeSplash from './HomeSplash';

import ForgotPassword from './ForgotPassword';
function PrivateRoute(props) {
    return props.user ?
        <Route {...{ ...props }} /> : <Login />

}

export function AuthRouteSwitch(props) {
    return (
        <div className="App">
            <Router>
                <Switch>
                    <PrivateRoute user={props.user} exact path="/login">
                        <DashBoard {...{ ...props }} />
                    </PrivateRoute>
                    <PrivateRoute user={props.user} exact path="/create-party">
                        <CreateParty {...{ ...props }} />
                    </PrivateRoute>
                    <Route exact path="/order-page">
                        <OrderPage {...{ ...props }} />
                    </Route>
                    <Route exact path="/">
                        <HomeSplash {...{ ...props }} />
                    </Route>
                    <Route exact path="/forgot-password">
                        <ForgotPassword {...{ ...props }} />
                    </Route>
                    <Route exact path="/view-orders">
                        <ViewOrders {...{ ...props }} />
                    </Route>
                    <PrivateRoute user={props.user} exact path="/view-orders-by-store">
                        <ViewOrdersByStore {...{ ...props }} />
                    </PrivateRoute>
                </Switch>
            </Router>
        </div>
    );
}




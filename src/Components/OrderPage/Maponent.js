import React, { useState } from 'react';
import { withGoogleMap, GoogleMap, Marker } from "react-google-maps";
const { InfoBox } = require("react-google-maps/lib/components/addons/InfoBox");

function InfoMarker({place,addPlace}) {

    const { formatted_address, formatted_phone_number, name, website } = place

    const [showMaker, setshowMaker] = useState(undefined);
    const [holdMaker, setHoldMarker] = useState(undefined);

    return (
        <>
            {(holdMaker || showMaker) &&
                <InfoBox
                    position={place.geometry.location}
                    options={{
                        closeBoxURL: ``,
                        enableEventPropagation: true,
                        disableAutoPan: true,
                    }}
                >
                    <div  style={{ backgroundColor: `white`, opacity: 0.8, padding: `12px` }}>
                        <div  className='info-box' style={{ fontSize: `16px`, fontColor: `#08233B`,width:100 }}>
                            <p className='bolder-text' >{name}</p>
                            <p>{formatted_address}</p>
                            {/* <p>{formatted_phone_number}</p>
                            <p>{website}</p> */}
                        </div>
                    </div>
                </InfoBox>

            }

            <Marker
                onClick={()=>{
                    addPlace(place)
                }}
                onDblClick={(event) => { setHoldMarker(!holdMaker) }}
                onMouseOut={(event) => { if (showMaker) { setshowMaker(false) } }}
                onMouseOver={(event) => { if (!showMaker) { setshowMaker(true) } }}
                position={{ ...place }}
            />
        </>
    )
}

export const Maponent = withGoogleMap((props) => {
    const [mapRef, setmapRef] = useState(undefined);

    return (
        <GoogleMap
            ref={setmapRef}
            center={props.center}
            zoom={props.zoom}
        >
            {props.homeMarker && (<Marker icon="/baseline_home_black_24dp.png" position={{ ...props.homeMarker }} />)}
            {props.mapPlaces.map(place => (<InfoMarker key={place.place_id} {...{ place,addPlace:props.addPlace}} />

            ))}
        </GoogleMap>
    );
});

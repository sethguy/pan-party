import React, { useState, useEffect } from 'react';
import { Maponent } from './Maponent';
import { Searchonent } from './Searchonent';
import { MapSearchonent } from './MapSearchonent';
import firebase from '../../firebase';
import { useAlert } from 'react-alert'
import ReactLoading from "react-loading";

const queryString = require('query-string');

async function submitOrder(props) {
  const ordersRef = firebase.firestore().collection('panOrders');
  const { pac, alert } = props;
  const oneRef = ordersRef.doc();
  const result = await oneRef.set({
    ...pac,
  });
  console.log("submitOrder -> oneRef", oneRef)
  alert.show('order submitted thanks!')

}


var scriptjs = require("scriptjs")

async function go(setloaded) {
  await new Promise((resolve, reject) => scriptjs('https://maps.googleapis.com/maps/api/js?key=AIzaSyC8MF5bOqi5K2VyRlcC6IQP5YCpVKQK35U&v=3.exp&libraries=,geometry,drawing,places,', resolve)
  );

  setloaded(true)
}

function OrderPage(props) {
  const params = queryString.parse(window.location.search)
  const { partyId } = params
  const [center, setcenter] = useState({
    lat: 38.8937796,
    lng: -77.1550039
  });

  const [zoom, setZoom] = useState(9)
  const [placeChoices, setPlacesChoices] = useState([])
  const alert = useAlert()

  const [mapSearchbounds, setmapSearchbounds] = useState(undefined);
  const [mapPlaces, setMapPlaces] = useState([]);
  const [homeMarker, sethomeMarker] = useState(undefined);
  const [loaded, setloaded] = useState(false);


  const [loading, setLoading] = useState(false);

  useEffect(() => {
    go(setloaded)
  }, []);

  return (

    <div

      className="h-100 d-flex flex-column">


      <header className="p-1">

        <h1 className="d-flex justify-content-between">
          <span>Order Form</span>
          <div>
            <p className="info-text"> fill out form to place order </p>
            <p className="info-text"> search for restaurants </p>
            <p className="info-text"> hover over markers for info </p>

            <p className="info-text"> click marker to  select restaurants options for order </p>

          </div>
        </h1>


      </header>


      <form
        onSubmit={(event) => {
          event.preventDefault();
          const raw = {};
          for (let index = 0; index < event.target.elements.length; index++) {
            const element = event.target.elements[index];
            if (element.name) {
              raw[element.name] = element.value;
            }
          }

          const pac = {
            ...raw,
            placeChoices: placeChoices.map(({ formatted_address, formatted_phone_number, name, website, id, lat, lng, place_id }) =>
              ({ formatted_address, formatted_phone_number, name, website, id, lat, lng, place_id })),
            partyId,
          }
          console.log("OrderPage -> pac", pac)

          submitOrder({ pac, alert, ...props })

        }}
        className="flex-1 scroll-y row no-gutters">

        {loaded &&

          <div className="col-md-6">
            <div
              className="h-100 d-flex flex-column px-1"
            >
              <label className="text-left m-0" htmlFor="address">type address</label>
              <Searchonent
                onPlaceSelect={(place) => {
                  const bounds = new window.google.maps.LatLngBounds();
                  if (place.geometry.viewport) {
                    bounds.union(place.geometry.viewport)
                  } else {
                    bounds.extend(place.geometry.location)
                  }
                  setmapSearchbounds(bounds)
                  setcenter(bounds.getCenter())
                  setZoom(bounds.getZoom ? bounds.getZoom() : (zoom || 8))

                  sethomeMarker(place)
                  console.log("OrderPage -> place", place)
                }}
                name="address"
                placeholder="address"
                loadingElement={<div style={{ height: `100%` }} />}
              />
              <label className="text-left m-0" htmlFor="order">type email address</label>
              <textarea name="email" className="form-control input-lg mb-1"></textarea>
              <label className="text-left m-0" htmlFor="order">type order</label>
              <textarea name="order" className="form-control input-lg flex-1 mb-1"></textarea>
            </div>
          </div>
        }
        {loaded &&
          <div className="d-flex flex-column col-md-6 px-1">
            <label className="text-left m-0" htmlFor="address">type restaurant name</label>
            <MapSearchonent
              setLoading={setLoading}
              bounds={mapSearchbounds}
              onPlaceSelect={(places) => {
                const bounds = new window.google.maps.LatLngBounds();
                places.forEach(place => {
                  if (place.geometry.viewport) {
                    bounds.union(place.geometry.viewport)
                  } else {
                    bounds.extend(place.geometry.location)
                  }
                });
                setcenter(bounds.getCenter())
                setZoom(bounds.getZoom ? bounds.getZoom() : (zoom || 8))


                setMapPlaces(places)
              }}
              name="placename"
              placeholder="restaurant"
              loadingElement={<div style={{ height: `100%` }} />}
            />
            {
              loading
                ?
                <div style={{ minHeight: 300 }} className='flex-1 d-flex justify-content-center h-100 align-items-center' >
                  <ReactLoading type="spinningBubbles" color="black" />
                </div>
                :
                <Maponent
                  addPlace={(place) => {
                    const dups = placeChoices.filter(ch => ch.id === place.id)
                    if (!dups.length) {
                      setPlacesChoices([...placeChoices, place])

                    }
                  }}
                  mapPlaces={mapPlaces}
                  homeMarker={homeMarker}
                  center={center}
                  zoom={zoom}
                  loadingElement={<div style={{ height: `100%` }} />}
                  containerElement={<div style={{ minHeight: 300 }} className='flex-1' />}
                  mapElement={<div style={{ height: `100%` }} />}
                />

            }



            <div style={{ maxHeight: '30%' }} className="place-choice-box scroll-y" >
              {
                placeChoices.map((choice) => {

                  const { formatted_address, formatted_phone_number, name, website, id, lat, lng, place_id } = choice
                  return (
                    <div className="my-1 d-flex justify-content-between">
                      <div className="flex-1">
                        <p className="m-0 bolder-text little-size">{name}</p>
                        <p className="m-0 little-size">{formatted_address}</p>
                        <a href={website} className="m-0 little-size">website</a>
                        <hr />
                      </div>

                      <div>

                        <i onClick={() => {
                          setPlacesChoices(placeChoices.filter(ch => ch.id !== choice.id))


                        }} className="fa fa-times text-danger"></i>
                      </div>
                    </div>
                  )
                })
              }

            </div>
            <input type="submit" className="btn btn-success my-1" />

          </div>
        }
      </form>

    </div>

  );
}

export default OrderPage;

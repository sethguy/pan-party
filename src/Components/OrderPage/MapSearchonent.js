import React, { useState } from 'react';
const { StandaloneSearchBox } = require("react-google-maps/lib/components/places/StandaloneSearchBox");

const wait = (ms) => new Promise(res => setTimeout(res, ms));

function getDetails(place, status) {

    return new Promise((resolve, reject) => {
        var request = {
            placeId: place.place_id,
            fields: ['name', 'formatted_phone_number', 'formatted_address', 'website']
        };
        var service = new window.google.maps.places.PlacesService(document.createElement('div'));
        service.getDetails(request, function (details, status) {
            if (status == window.google.maps.places.PlacesServiceStatus.OK) {

                const { name, formatted_address, formatted_phone_number, website } = details
                resolve({
                    formatted_address, formatted_phone_number, name, website,
                    ...place
                })
            } else {

                resolve({
                    ...place
                })
            }

        });
    })
}


async function getPlaceDetails(props) {
    const { searchRef,setLoading } = props;
    const places = searchRef.getPlaces();

    const mapped = places.map(pl => {
        return {
            ...pl,
            lat: pl.geometry.location.lat(),
            lng: pl.geometry.location.lng()
        };
    })
        .filter(place => {
            const { formatted_address, formatted_phone_number, name, website, id, lat, lng, place_id } = place
            return true
        })
        setLoading(true)
    const allStufff = [];
    while (allStufff.length < mapped.length) {
        await wait(100)
        const details = await getDetails(mapped[allStufff.length])
        allStufff.push(details)
    }
    setLoading(false)


    const done = allStufff.filter(place => {
        const { formatted_address, formatted_phone_number, name, website, id, lat, lng, place_id } = place
        return formatted_phone_number && name
    })
    props.onPlaceSelect(done)


}

export const MapSearchonent = (props) => {

    const [searchRef, setsearchRef] = useState(undefined);
    const [address, setaddress] = useState([]);
    return (
        <StandaloneSearchBox
            ref={setsearchRef}
            bounds={props.bounds}
            onPlacesChanged={(event) => {
                getPlaceDetails({ searchRef, ...props })
            }}>
            <textarea
                value={address}
                onChange={(event) => {
                    setaddress(event.target.value)
                }}
                name={props.placeholder}
                placeholder={props.placeholder}
                className="form-control border border-primary input-lg mb-1"></textarea>
        </StandaloneSearchBox>
    );
};

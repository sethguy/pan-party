import React, { useState } from 'react';
const { StandaloneSearchBox } = require("react-google-maps/lib/components/places/StandaloneSearchBox");
export const Searchonent = (props) => {
    const [searchRef, setsearchRef] = useState(undefined);
    const [address, setaddress] = useState([]);
    return (
        <StandaloneSearchBox
            ref={setsearchRef}
            bounds={props.bounds}
            onPlacesChanged={(event) => {
                const places = searchRef.getPlaces();
                const mapped = places.map(pl => {
                    return {
                        ...pl,
                        lat: pl.geometry.location.lat(),
                        lng: pl.geometry.location.lng()
                    };
                });

                if (mapped.length && mapped[0].formatted_address) {
                    setaddress(mapped[0].formatted_address)

                    props.onPlaceSelect({
                        ...mapped[0],
                    })
                }

            }}>
            <textarea
                value={address}
                onChange={(event) => {
                    setaddress(event.target.value)
                }}
                name={props.placeholder}
                placeholder={props.placeholder}
                className="form-control input-lg mb-1"></textarea>
        </StandaloneSearchBox>
    );
};

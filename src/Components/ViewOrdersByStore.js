import React, { useState } from 'react';
import { useGetPanOrders } from '../hooks/useGetPanOrders';
import firebase from '../firebase';

const queryString = require('query-string');


function getStoreReport(props) {
  const { panOrders = [] } = props;

  const storeReport = panOrders.reduce((report, order) => {

    const { placeChoices = [] } = order;

    const [first] = placeChoices;

    const { formatted_address, formatted_phone_number, id, place_id, website, name } = first;

    const existing = report[place_id] || { orders: [] };


    return {
      [place_id]: {
        ...existing,
        place: { ...first },
        orders: [
          ...existing.orders,
          order,
        ]
      },
      ...report,
    }
  }, {})

  const storeReportList = Object.keys(storeReport).map(place_id => storeReport[place_id])

  return { storeReport, storeReportList }

}
function ViewOrdersByStore(props) {
  const { panOrders = [] } = props;
  const params = queryString.parse(window.location.search)
  const { partyId } = params;

  const { storeReportList = [] } = getStoreReport(props);

  const [selectedStoreOrderReport, setSelectedStoreOrderReport] = useState(undefined)
  useGetPanOrders({ ...props, partyId })
  return (
    <div className="App d-flex h-100 flex-column">

      <header className="p-1">

        <h1 className="d-flex justify-content-between">
          <span>View Orders By Store</span>

          <button
            onClick={() => {
              firebase.auth().signOut()
            }}
            className="btn btn-info btn-sm"
          >
            sign out
          </button>

        </h1>

        <a className="bold-weight" href={'/login'} > dashboard </a>
      </header>
      <div className="h-100 scroll-y row no-gutters px-1">

        <div className="col-md-6">
          {
            storeReportList
              .map(storeOrderReport => {
                const { place } = storeOrderReport;
                const { formatted_address, formatted_phone_number, id, place_id, website, name } = place;
                return (
                  <div
                    className="btn p-0"
                    onClick={(event) => {
                      setSelectedStoreOrderReport(storeOrderReport)
                    }}
                    key={place_id}>
                    <p className="bold-weight">{name}</p>
                    <hr />
                  </div>
                )
              })
          }
        </div>

        <div className="col-md-6">

          {
            selectedStoreOrderReport &&
            <div
            >
              <p className="bold-weight m-0">{selectedStoreOrderReport.place.formatted_phone_number}</p>
              <a className="little-size" href={selectedStoreOrderReport.place.website} >website</a>
              <p className="little-size" >{selectedStoreOrderReport.place.formatted_address}</p>
              <hr />
              <div>
                {
                  selectedStoreOrderReport.orders.map(panOrder => {
                    const { email, order, address } = panOrder;
                    return (
                      <div key={panOrder.id}>
                        <p className="m-0 bold-weight">{order}</p>
                        <p className="m-0 bolder-text">{address}</p>
                        <p className="m-0">{email}</p>
                      </div>
                    )
                  })
                }
              </div>
              <hr />
            </div>
          }

        </div>

      </div>
    </div>
  );
}

export default ViewOrdersByStore;

import React, { useState } from 'react';
import { useGetPanOrders } from '../hooks/useGetPanOrders';
import firebase from '../firebase';

const queryString = require('query-string');

function ViewOrders(props) {
  const { panOrders = [] } = props;
  const params = queryString.parse(window.location.search)
  const { partyId } = params;

  const [selectedPanOrder, setSelectedPanOrder] = useState(undefined)
  useGetPanOrders({ ...props, partyId })
  return (
    <div className="App d-flex h-100 flex-column">

      <header className="px-1">

        <h1 className="d-flex justify-content-between">
          <span>View Orders</span>

          <button
            onClick={() => {
              firebase.auth().signOut()
            }}
            className="btn btn-info btn-sm"
          >
            sign out
          </button>

        </h1>
        <a className="bold-weight" href={'/login'} > dashboard </a>
      </header>

      <div className="h-100 row no-gutters px-1">

        <div className="col-md-6 scroll-y">
          {
            panOrders
              .map(panOrder => {
                return (
                  <div
                    className="btn p-0"
                    onClick={(event) => {
                      setSelectedPanOrder(panOrder)
                    }}
                    key={panOrder.id}>
                    <p className="bold-weight m-0">{panOrder.email}</p>
                    <hr />
                  </div>
                )
              })
          }
        </div>

        <div className="col-md-6 scroll-y">

          {
            selectedPanOrder &&
            <div
            >
              <p className="bold-weight m-0">{selectedPanOrder.order}</p>
              <p className="m-0 bolder-text">{selectedPanOrder.address}</p>
              <hr />

              <div>
                {
                  selectedPanOrder.placeChoices.map(placeChoice => {

                    const { formatted_address, formatted_phone_number, name, website, } = placeChoice;

                    return (
                      <div>
                        <p className="m-0">{name}</p>
                        <p className="m-0 bold-weight">{formatted_phone_number}</p>
                        <p className="m-0 little-size">{formatted_address}</p>
                        <a href="website" className="m-0 little-size">website</a>
                        <br />
                      </div>
                    )
                  })
                }
              </div>
              <hr />
            </div>
          }

        </div>

      </div>
    </div>
  );
}

export default ViewOrders;

import React from 'react';
import firebase from '../../firebase';

function LoginForm(props) {

  return (
    <form
      onSubmit={(event) => {
        event.preventDefault();
        const raw = {};
        for (let index = 0; index < event.target.elements.length; index++) {
          const element = event.target.elements[index];
          if (element.name) {
            raw[element.name] = element.value;
          }
        }
        const { email, password } = raw;
        if (email && password) {
          firebase.auth().signInWithEmailAndPassword(email, password)
        }

      }}
    >
      <label className="m-0 p-0" htmlFor="email">email</label>
      <input type="text" name="email" className="form-control input-lg" />
      <label className="m-0 p-0" htmlFor="password">password</label>
      <input type="password" name="password" className="form-control input-lg" />
      <br />
      <input className="btn btn-info boldest-text" type="submit" value="login (email)" />
      <button
        onClick={(event) => {
          event.preventDefault();

          const raw = {};
          for (let index = 0; index < event.target.parentNode.elements.length; index++) {
            const element = event.target.parentNode.elements[index];
            if (element.name) {
              raw[element.name] = element.value;
            }
          }

          const { email, password } = raw;

          if (email && password) {
            firebase.auth().createUserWithEmailAndPassword(email, password)
          }

        }}
        className="btn btn-info boldest-text mx-1" > Sign Up (Email)</button>
      <hr />
      <button
        onClick={() => {
          var provider = new firebase.auth.GoogleAuthProvider();
          firebase.auth().signInWithPopup(provider)

        }}
        className="btn btn-info boldest-text" > login with google</button>

<a
  href="/forgot-password"
        className="btn " > forgot password</a>
    </form>
  )

}

function Login(props) {
  return (
    <div className="d-flex h-100 flex-column">


      <div className='new-header d-flex p-1'>
        <div className="new-header-card">
        <span className="bold-weight new-header-span">Login Page </span>
        </div>
        <div className="new-header-card">
          <span className='header-span' >PAN PARTY</span>
        </div>
        <div className="new-header-card d-flex justify-content-end">
          <img style={{ height: 50, width: 50 }} src="/ms-icon-310x310.png" />
        </div>
      </div>

      <div className="flex-1 scroll-y d-flex flex-column justify-content-center align-items-center">
        <LoginForm />
      </div>
    </div>
  );
}

export default Login;

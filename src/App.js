import React from 'react';
import { useInitStore } from './hooks/useInitStore'
import ReactLoading from "react-loading";

import './App.css'
import { AuthRouteSwitch } from './Components/AuthRouteSwitch';

import { transitions, positions, Provider as AlertProvider } from 'react-alert'
import AlertTemplate from 'react-alert-template-basic'

const options = {
  // you can also just use 'bottom center'
  position: positions.MIDDLE,
  timeout: 5000,
  offset: '30px',
  // you can also just use 'scale'
  transition: transitions.SCALE
}

function App(props) {

  const myHookStore = useInitStore()

  return (
    <div className="App">
      {
        myHookStore.loading
          ?
          <div className='d-flex justify-content-center h-100 align-items-center' >
            <ReactLoading type="spinningBubbles" color="black" />
          </div>
          :
          <AlertProvider template={AlertTemplate} {...options}>
            <AuthRouteSwitch {...{ ...myHookStore }} />
          </AlertProvider>
      }
    </div>
  );
}

export default App;

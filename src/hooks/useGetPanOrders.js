
import firebase from '../firebase';
import { useState, useEffect } from 'react';

async function getPanOrders(props) {
    const { setPanOrders, partyId, auth } = props;
        const partiesRef = firebase.firestore().collection('panOrders');
        const { docs } = await partiesRef.where('partyId','==',partyId).get();
        const datalist = docs.map(doc => ({
            ...doc.data(),
            id: doc.id,
        }));
        setPanOrders(datalist);
    
}


export function useGetPanOrders(props) {
    const { partyId } = props;

    useEffect(() => {
        getPanOrders(props)
    }, [partyId]);

}

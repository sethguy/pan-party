
import firebase from '../firebase';
import { useEffect } from 'react';

export function useInitWatchAuth(props) {
  const { setUser, setLoading } = props;
  useEffect(() => {
    firebase.auth().onAuthStateChanged(function (user) {
      setLoading(false)
      var user = firebase.auth().currentUser;
      if (user) {
        setUser(user)
      } else {
        setUser(undefined)
      }
    });
  }, [])
}

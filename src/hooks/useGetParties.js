
import firebase from '../firebase';
import { useEffect } from 'react';

async function getParties(props) {
    const { setParties, user, auth } = props;
    if (user) {
        const partiesRef = firebase.firestore().collection('panParties');
        const { docs } = await partiesRef.where('hostId','==',user.uid).get();
        const datalist = docs.map(doc => ({
            ...doc.data(),
            id: doc.id,
        }));
        setParties(datalist);
    }
}

export function useGetParties(props) {
    const { user } = props;
    useEffect(() => {
        getParties(props)
    }, [user]);

}

import { useState } from 'react';
import { useInitWatchAuth } from './useInitWatchAuth'


export function useInitStore(props) {

  const [user, setUser] = useState(undefined);
  const [parties, setParties] = useState([]);
  const [panOrders, setPanOrders] = useState([]);
  const [loading, setLoading] = useState(true);
  useInitWatchAuth({ setUser,setLoading })

  return {loading, setLoading, user, setUser, parties, setParties, panOrders, setPanOrders };
}

import firebase from "firebase";

// See firebase setup in above google firebase documentation url
export const config = {
    apiKey: "AIzaSyA1BxZkymqrIC9ORv--Wn5D05JJRKVIL4g",
    authDomain: "pan-party.firebaseapp.com",
    databaseURL: "https://pan-party.firebaseio.com",
    projectId: "pan-party",
    storageBucket: "pan-party.appspot.com",
    messagingSenderId: "279799895581",
    appId: "1:279799895581:web:81be795b29f5b5821118f4",
    measurementId: "G-ZPJQH2SJPC"
};

firebase.initializeApp(config);
export default firebase;